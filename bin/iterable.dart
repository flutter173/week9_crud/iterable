import 'package:iterable/iterable.dart' as iterable;

void main(List<String> arguments) {
  print('Hello world: ${iterable.calculate()}!');
}
